#!/bin/bash

# Define variables
DOMAIN="jhedfactolerin.com"
EMAIL="jhedfactolerin@gmail.com"
WEBROOT="/usr/share/nginx/html"

# Start NGINX in the background for HTTP traffic
echo "Starting NGINX to allow HTTP validation..."
nginx &

# Wait for NGINX to start
sleep 5

# Request SSL certificate using Certbot
echo "Generating SSL certificate for $DOMAIN..."
certbot certonly --webroot -w $WEBROOT \
    -d $DOMAIN --non-interactive --agree-tos --email $EMAIL

# Replace HTTP NGINX config with HTTPS config
echo "Configuring NGINX for HTTPS..."
cat > /etc/nginx/conf.d/default.conf <<EOF
server {
    listen 443 ssl;
    server_name $DOMAIN;

    ssl_certificate /etc/letsencrypt/live/$DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$DOMAIN/privkey.pem;

    root $WEBROOT;
    index index.html;

    location / {
        try_files \$uri \$uri/ =404;
    }
}

server {
    listen 80;
    server_name $DOMAIN;

    # Redirect all HTTP to HTTPS
    return 301 https://\$host\$request_uri;
}
EOF

# Stop temporary NGINX and restart with HTTPS
echo "Restarting NGINX with HTTPS configuration..."
nginx -s stop
nginx -g "daemon off;"
FROM node:14.17.3-alpine as build

WORKDIR /code
COPY package.json package-lock.json ./

RUN npm ci --production

COPY . .

# Increase memory limit for build
RUN npm run build

# NGINX Web Server
FROM nginx:stable-alpine as prod

# Install Certbot and required tools
RUN apk add --no-cache certbot openssl bash

# Copy build files
COPY --from=build /code/build /usr/share/nginx/html

# Copy NGINX configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Script for certificate generation
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

EXPOSE 80 443

CMD ["/entrypoint.sh"]
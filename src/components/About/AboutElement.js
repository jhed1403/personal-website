import styled from 'styled-components'


export const AboutContainer = styled.div`
    min-height: 100vh;
    background-color: #577897;
    width: 100%;
`

export const AboutImageWrapper = styled.div`
    width: 100%;
    padding-top: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const AboutImage = styled.img`
    width: 180px;
    height: 180px;
    margin-top: 100px;
`

export const AboutMyNameWrapper = styled.div`
    height: 150px;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const AboutMyName = styled.p`
    color: white;
    font-size: 2rem;
    padding-left: 3%;
    padding-right: 3%;
    text-align: center;

    @media screen and (max-width: 768px){
        font-size: 1.5rem;
    }
`

export const AboutCurlyRight = styled.h1`
    color: white;
    font-size: 3.5rem;
    font-weight: 400;

    @media screen and (max-width: 768px){
        font-size: 2.8rem;
    }
`

export const AboutCurlyLeft = styled.h1`
    color: white;
    font-size: 3.5rem;
    font-weight: 400;

    @media screen and (max-width: 768px){
        font-size: 2.8rem;
    }
`

export const AboutNameSpan = styled.span`
    font-weight: 700;
`

export const AboutDescriptionWrapper = styled.div`
    display: flex;
    text-align: center;
    justify-content: center;
`

export const AboutDescription = styled.p`
    color: white;
    max-width: 700px;
    padding-left: 25px;
    padding-right: 25px;
    font-size: 1.5rem;
`

export const AboutWrapper = styled.div`
    max-width: 1300px;
    margin: 0 auto;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    align-items: center;
    grid-gap: 20px;
    padding-left: 0 50px;
    margin-top: 70px;
    

    @media screen and (max-width: 768px){
        grid-template-columns: 1fr;
        padding-bottom: 70px;
    }
    
`
export const AboutCard = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
`

export const AboutH2 = styled.p`
    color: white;
    margin-bottom: 10px;
    font-size: 1.7rem;
    font-weight: 700;
`

export const AboutIconWrapper = styled.div`
    height: 140px;
    display: flex;
    align-items: center;
`

export const AboutIconEducation = styled.img`
    width: 250px;
    height: 137px;
    margin-bottom: 10px;
`

export const AboutIconProgramming = styled.img`
    width: 300px;
    height: 132px;
    margin-bottom: 10px;
`

export const AboutIconPCBuilding = styled.img`
    width: 250px;
    height: 137px;
    margin-bottom: 10px;
`

export const AboutP = styled.p`
    color: white;
    text-align: center;
    margin-bottom: 10px;

    @media screen and (max-width: 768px){
        padding-left: 15px;
        padding-right: 15px;
    }
`
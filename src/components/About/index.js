import React from 'react'
import MyImage from '../../images/my_picture.png'
import SchoolLogo from '../../images/Sheridan_College_Logo.png'
import ProgrammingLogo from '../../images/programming_languages.png'
import PCBuildingLogo from '../../images/Hardware-components.jpg'
import TextAnimation from '../Extras/TextAnimation'

import {
    AboutContainer,
    AboutImageWrapper,
    AboutImage,
    AboutMyNameWrapper,
    AboutMyName,
    AboutCurlyLeft,
    AboutCurlyRight,
    AboutNameSpan,
    AboutDescriptionWrapper,
    AboutDescription,
    AboutWrapper,
    AboutCard,
    AboutH2,
    AboutIconWrapper,
    AboutIconEducation,
    AboutIconProgramming,
    AboutIconPCBuilding,
    AboutP
} from './AboutElement'

const About = () => {
    return (
        <>
            <AboutContainer id="about">
                <AboutImageWrapper>
                    <AboutImage src={MyImage} />
                </AboutImageWrapper>

                <AboutMyNameWrapper>
                    <AboutCurlyLeft>&#123;</AboutCurlyLeft>

                    <AboutMyName>Hey there! I am <AboutNameSpan>Jhed</AboutNameSpan> <br /> and I am a <AboutNameSpan><TextAnimation></TextAnimation></AboutNameSpan>.</AboutMyName>

                    <AboutCurlyRight>&#125;</AboutCurlyRight>
                </AboutMyNameWrapper>

                <AboutDescriptionWrapper>
                    <AboutDescription>
                        Learning and building new things satifies my hunger.
                    </AboutDescription>
                </AboutDescriptionWrapper>

                <AboutWrapper>
                    <AboutCard>
                        <AboutH2>Education</AboutH2>
                        <AboutIconWrapper>
                            <AboutIconEducation src={SchoolLogo} />
                        </AboutIconWrapper>
                        <AboutP>
                            I completed an advanced diploma in Systems Analysis at Sheridan College, where I gained hands-on experience in software and web application development using languages and frameworks such as Java, C#, and .NET.
                        </AboutP>
                    </AboutCard>

                    <AboutCard>
                        <AboutH2>Programming</AboutH2>
                        <AboutIconWrapper>
                            <AboutIconProgramming src={ProgrammingLogo} />
                        </AboutIconWrapper>
                        <AboutP>
                            I have been fascinated by computers since childhood. Over the years, I have learned a variety of programming languages and tools through my involvement in numerous school, personal, and professional projects.
                        </AboutP>
                    </AboutCard>

                    <AboutCard>
                        <AboutH2>PC Building</AboutH2>
                        <AboutIconWrapper>
                            <AboutIconPCBuilding src={PCBuildingLogo} />
                        </AboutIconWrapper>
                        <AboutP>
                            My hobby is building and fixing computer.
                            I built my very first gaming computer in 2018 and from then I have been helping my family and friends in building and fixing their personal computers.
                        </AboutP>
                    </AboutCard>
                </AboutWrapper>
            </AboutContainer>
        </>
    )
}

export default About

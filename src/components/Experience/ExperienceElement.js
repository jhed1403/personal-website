import styled from "styled-components";

export const ExperienceContainer = styled.div`
    background-color: #FFF;
    align-items: center;
    width: 100%;
`

export const ExperienceH1Container = styled.div`
    text-align: center;
    padding-bottom: 50px;
`

export const ExperienceH1 = styled.p`
    padding-top: 50px;
    padding-bottom: 20px;
    font-size: 2.5rem;
    font-weight: 700;
`

export const ExperienceResumeButton = styled.a`
    text-align: center;

    border-radius: 30px;
    background: #577897;
    white-space: nowrap;
    padding: 10px 22px;
    color: #fff;
    font-size: 16px;
    outline: none;
    border: none;
    cursor: pointer;
    transition: all 0.2 ease-in-out;
    text-decoration: none;

    &:hover {
        transition: all 0.2s ease-in-out;
        background: #d0d2d3;
        color: #FFF;
    }

`

import React from 'react'
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import Resume from '../../files/resume.pdf'
import {
    ExperienceContainer,
    ExperienceH1Container,
    ExperienceH1,
    ExperienceResumeButton
} from './ExperienceElement'

const Experience = () => {
    return (
        <>
            <ExperienceContainer id="experience">
                <ExperienceH1Container>
                    <ExperienceH1>Experience</ExperienceH1>
                    <ExperienceResumeButton primary="primary" href={Resume} target="_blank">View Resume</ExperienceResumeButton>
                </ExperienceH1Container>

                <VerticalTimeline lineColor={"#d0d2d3"} layout={"1-column-left"}>
                    <VerticalTimelineElement
                        className="vertical-timeline-element--work"
                        contentStyle={{ background: '#577897', color: '#fff' }}
                        contentArrowStyle={{ borderRight: '7px solid  #577897' }}
                        date="October 2023 - Present"
                        iconStyle={{ background: '#577897', color: '#d0d2d3' }}
                    >
                        <h3 className="vertical-timeline-element-title">F56</h3>
                        <h5 className="vertical-timeline-element-subtitle">Senior Software Developer</h5>
                        <p>
                            <b>Knowledge Gained:</b> Typescript, Angular, .NET Core, Azure Services (Functions, ServiceBus, Storage, AppServices, etc.), Azure Pipelines, Hangfire
                        </p>
                    </VerticalTimelineElement>

                    <VerticalTimelineElement
                        className="vertical-timeline-element--work"
                        contentStyle={{ background: '#577897', color: '#fff' }}
                        contentArrowStyle={{ borderRight: '7px solid  #577897' }}
                        date="February 2022 - October 2023"
                        iconStyle={{ background: '#577897', color: '#d0d2d3' }}
                    >
                        <h3 className="vertical-timeline-element-title">SPS Commerce</h3>
                        <h5 className="vertical-timeline-element-subtitle">Software Developer</h5>
                        <p>
                            <b>Knowledge Gained:</b> Typescript, ReactJS, .NET Core, .NET Framework, AWS (State Machine, S3, SQS, Dynamo DB, etc.), Snowflake, Azure Pipelines, SQL, MySQL, Grafana, SumoLogic, Sentry
                        </p>
                    </VerticalTimelineElement>
                    <VerticalTimelineElement
                        className="vertical-timeline-element--work"
                        contentStyle={{ background: '#577897', color: '#fff' }}
                        contentArrowStyle={{ borderRight: '7px solid  #577897' }}
                        date="September 2017 - February 2022"
                        iconStyle={{ background: '#577897', color: '#d0d2d3' }}
                    >
                        <h3 className="vertical-timeline-element-title">Addison Fleet</h3>
                        <h5 className="vertical-timeline-element-subtitle">Software Developer</h5>
                        <p>
                            <b>Knowledge Gained:</b> PHP, Codeigniter, MySQL, Git, C#, TSQL, Python, Selenium, ASP.Net WebAPI, Dynamics CRM, Window Server Administration
                        </p>
                    </VerticalTimelineElement>

                    <VerticalTimelineElement
                        className="vertical-timeline-element--work"
                        contentStyle={{ background: '#577897', color: '#fff' }}
                        contentArrowStyle={{ borderRight: '7px solid  #577897' }}
                        date="January 2017 - April 2017"
                        iconStyle={{ background: '#577897', color: '#d0d2d3' }}
                    >
                        <h3 className="vertical-timeline-element-title">Scotiabank</h3>
                        <h4 className="vertical-timeline-element-subtitle">Data Scientists - COOP</h4>
                        <p>
                            <b>Knowledge Gained:</b> Docker, Jenkins, Agile Methodology, GitLab CI/CD, Cloud Infrastructure, Flask, Shell Scripting
                        </p>
                    </VerticalTimelineElement>
                </VerticalTimeline>
            </ExperienceContainer>
        </>
    )
}

export default Experience

import React from 'react'
import { SidebarContainer, Icon, CloseIcon, SidebarWrapper, SidebarMenu, SidebarLink } from './SidebarElement'


const Sidebar = ({isOpen, toggle}) => {
    return (
        <SidebarContainer isOpen={isOpen} onClick={toggle}>
            <Icon onClick={toggle}>
                <CloseIcon /> 
            </Icon>
            <SidebarWrapper>
                <SidebarMenu>
                    {/* <SidebarLink to="home" onClick={toggle}>
                        Home
                    </SidebarLink> */}
                    <SidebarLink to="about" smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}
                            onClick={toggle}>
                        About
                    </SidebarLink>
                    <SidebarLink to="experience" smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}
                            onClick={toggle}>
                        Experience
                    </SidebarLink>
                    <SidebarLink to="connect" smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}
                            onClick={toggle}>
                        Connect
                    </SidebarLink>
                    <SidebarLink to="Resume" smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}
                            onClick={toggle}>
                        Resume
                    </SidebarLink>
                </SidebarMenu>
            </SidebarWrapper>
        </SidebarContainer>
    )
}

export default Sidebar

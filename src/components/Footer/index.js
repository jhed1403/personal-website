import React from 'react'
import {    FooterContainer,
            FooterH1 } from './FooterElement'

const Footer = () => {
    return (
        <>
            <FooterContainer>
                <FooterH1>© 2021 Jhed Factolerin <br />  <br /> All trademarks, logos and brand names are the property of their respective owners.</FooterH1>
            </FooterContainer>
        </>
    )
}

export default Footer

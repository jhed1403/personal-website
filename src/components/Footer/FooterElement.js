import styled from "styled-components";

export const FooterContainer = styled.div`
    background: #000;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 80px;
`

export const FooterH1 = styled.p`
    color: white;
    font-size: 10px;
    text-align: center;
    padding-left: 20px;
    padding-right: 20px;
`
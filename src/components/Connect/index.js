import React from 'react'
import { SiGmail, SiLinkedin } from 'react-icons/si'
import { RiGitlabFill } from 'react-icons/ri'
import {    ConnectContainer,
            ConnectH1Wrapper,
            ConnectH1,
            ConnectTagline,
            ConnectIconsWrapper,
            ConnectIcon,
            ConnectIconLink
        } from './ConnectElement'

const Connect = () => {
    return (
        <>
            <ConnectContainer id="connect">
                <ConnectH1Wrapper>
                    <ConnectH1>Let's build stuff together.</ConnectH1>
                    <br />
                    <ConnectTagline>Please do not hesitate to contact me.</ConnectTagline>
                    <br />
                    <br />
                    <ConnectIconsWrapper>
                        <ConnectIcon>
                            <ConnectIconLink href="mailto:jhedfactolerin@gmail.com" target="_blank"><SiGmail /></ConnectIconLink>
                        </ConnectIcon>

                        <ConnectIcon>
                            <ConnectIconLink href="https://www.linkedin.com/in/jhed-factolerin-51b19b127/" target="_blank"><SiLinkedin /></ConnectIconLink>
                        </ConnectIcon>
                        
                        <ConnectIcon>
                            <ConnectIconLink href="https://gitlab.com/jhed1403" target="_blank"><RiGitlabFill /></ConnectIconLink>
                        </ConnectIcon>
                    </ConnectIconsWrapper>
 
                </ConnectH1Wrapper>
            </ConnectContainer>
        </>
    )
}

export default Connect

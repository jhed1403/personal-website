import styled from "styled-components";

export const ConnectContainer = styled.div`
    min-height: 100vh;
    background-color: #577897;
    display: flex;
    align-items: center;
    width: 100%;
`

export const ConnectH1Wrapper = styled.div`
    height: 150px;
    width: 100%;
    align-items: center;
    justify-content: center;
`

export const ConnectH1 = styled.p`
    font-size: 3rem;
    text-align: center;
    color: white;
    width: 100%;
    padding-left: 15px;
    padding-right: 15px;
`

export const ConnectTagline = styled.p`
    font-size: 1rem;
    text-align: center;
    color: white;
    width: 100%;
`

export const ConnectIconsWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    width: 100%;
`

export const ConnectIcon = styled.div`
    padding-left: 15px;
    padding-right: 15px;
`

export const ConnectIconLink = styled.a`
    font-size: 3rem;
    cursor: pointer;
    color: white;
`
import React from "react";
import { useDencrypt } from "use-dencrypt-effect";

const values = ["developer", "builder", 'problem solver', "thinker", "quick learner"];

const TextAnimation = () => {

    const { result, dencrypt } = useDencrypt();

    React.useEffect(() => {

        let i = 0;
        const action = () => {
            dencrypt(values[i]);
            i = i === values.length - 1 ? 0 : i + 1;
        };

        action();

        const timerID = setInterval(action, 2500);

        return () => clearInterval(timerID);
        // eslint-disable-next-line
    }, []);

    return <>{result}</>;
};


export default TextAnimation
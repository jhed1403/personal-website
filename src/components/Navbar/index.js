import React from 'react'
import {FaBars} from 'react-icons/fa'
import Resume from '../../files/resume.pdf'
import {    Nav, 
            NavbarContainer, 
            MobileIcon, 
            NavMenu, 
            NavItem, 
            NavLinks, 
            NavLinksResume } from './NavbarElement'



const Navbar = ({ toggle }) => {
    return (
        <>
            <Nav>
                <NavbarContainer>
                    {/* <NavLogo to="/">JF</NavLogo> */}
                    <MobileIcon onClick={toggle}> 
                        <FaBars />
                    </MobileIcon>
                    <NavMenu>
                        {/* <NavItem>
                            <NavLinks to="project" 
                            smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}>Home</NavLinks>
                        </NavItem> */}
                        <NavItem>
                            <NavLinks to="about"
                            smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}>About</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to="experience"
                            smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}>Experience</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to="connect"
                            smooth={true}
                            duration={500}
                            spy={true}
                            exact="true"
                            offset={-80}>Connect</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinksResume href={ Resume } target="_blank">Resume</NavLinksResume>
                        </NavItem>
                    </NavMenu>
                </NavbarContainer>
            </Nav>
        </>
    )
}

export default Navbar

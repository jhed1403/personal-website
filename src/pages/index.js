import React, {useState} from 'react'
import Navbar from '../components/Navbar'
import Sidebar from '../components/Sidebar'
import About from '../components/About'
import Experience from '../components/Experience'
import Connect from '../components/Connect'
import Footer from '../components/Footer'

const Home = () => {

    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => {
        setIsOpen(!isOpen)
    }

    return (
        <>
            <Sidebar isOpen={isOpen} toggle={toggle}/>
            <Navbar toggle={toggle}/>  
            {/* <Project /> */}
            <About />
            <Experience />
            <Connect />
            <Footer />
        </>
    )
}

export default Home
